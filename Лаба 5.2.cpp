﻿// Лаба 5.2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include <algorithm>
#include <fstream>
#include <sstream>
#include <windows.h>


using namespace std;

string mas[5000];

int main() {
    setlocale(LC_ALL, "Russian");
    SetConsoleCP(1251);
    std::ifstream in("input.txt");
    std::ofstream out("output.txt");
    int n, x = 0;
    in >> n;
    string s;
    bool flag;
    while (in >> s) 
    {
        flag = true;
        for (int i = 0; i < s.length() - 1; i++) 
        {
            for (int j = i + 1; j < s.length(); j++) 
            {
                //cout << s[i] << " " << s[j] << endl;
                if (s[i] == s[j])
                    flag = false;
            }
        }
        if (flag) {
            mas[x] = s;
            x++;
        }
    }

    for (int i = 0; i < x - 1; i++) 
    {
        for (int j = i + 1; j < x; j++) 
        {
            if (mas[i].length() < mas[j].length())
                swap(mas[i], mas[j]);
        }
    }

    for (int i = 0; i < n; i++) 
    {
        out << mas[i] << " ";
    }

    return 0;
}